#### 介绍
图片相似度和文本相似度的实现

#### 图片和文本来源
https://www.3dmgame.com/games/pokemonswordshield/tj/

#### 说明
并非最优的相似度算法，需要安装pandas、numpy、jieba、opencv库。

仅提供学习。
